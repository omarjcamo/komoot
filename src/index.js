import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {configureStore} from './app/store/configureStore';
import './index.css';
import App from './app/layout/App';
import * as serviceWorker from './serviceWorker';

const rootRl = document.getElementById('root');

const store = configureStore();

let render = () => {
  ReactDOM.render(<Provider store={store}><App/></Provider>, rootRl);
};

if (module.hot) {
  module.hot.accept('./app/layout/App', () => {
    setTimeout(render)
  })
}
render();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
