import React, {Component} from 'react';
import ReactDomServer from 'react-dom/server';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {reorderMarkers, removeMarker} from "../sidebarActions";
import './Sidebar.css'
import {setMarker} from "../../map/mapActions";
// import Waypoint from "../Waypoint/Waypoint";

const placeholder = document.createElement("li");
placeholder.className = "waypoint-container placeholder";

class Sidebar extends Component {

  constructor(props) {
    super(props);
    this.state = {markers: props.markers};
    this.handleDeleteMarker = this.handleDeleteMarker.bind(this);
    this.generateGpxFile = this.generateGpxFile.bind(this);
  }

  render() {
    const {markers} = this.props;
    return (
      <aside className="sidebar">
        <h1>Route Builder</h1>
        <hr/>
        <ul className="waypoints-container"
            onDragOver={this.dragOver.bind(this)}>
          {
            markers && markers.map((marker, index) => {
              return marker && <li
                data-id={index}
                key={index}
                draggable="true"
                onDragEnd={this.dragEnd.bind(this)}
                onDragStart={(e) => this.dragStart(e, index)}
                className="waypoint-container">
                <img src="/assets/icon-menu.png" alt="Drag item" className="menu-icon"/>
                <div className="waypoint-content">Waypoint {index + 1} {' '}
                  <span className="coords">[{marker[0].toFixed(2)},{marker[1].toFixed(2)}]</span></div>
                <img src="/assets/icon-delete.png" alt="Delete item" className="delete-icon"
                     onClick={() => this.handleDeleteMarker(index)}/>
              </li>
            })
          }
        </ul>
        {
          markers.length > 0 &&
          <div className="button-container">
            <button onClick={this.generateGpxFile}>Download your Route</button>
          </div>
        }
      </aside>
    );
  }

  handleDeleteMarker(index) {
    this.props.removeMarker(index);
  }

  dragStart(e) {
    this.dragged = e.currentTarget;
    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData('text/html', this.dragged);
  }

  dragEnd() {
    this.dragged.style.display = 'flex';
    if (this.dragged.parentNode.contains(placeholder)) {
      placeholder.remove();
    }

    const from = Number(this.dragged.dataset.id);
    let to = Number(this.overId);
    this.props.reorderMarkers(from, to);
  }

  dragOver(e) {
    e.preventDefault();
    this.dragged.style.display = "none";
    if (e.target.className !== 'waypoint-container') return;
    this.overId = e.target.dataset.id;
    e.target.parentNode.insertBefore(placeholder, e.target);
  }

  generateGpxFile() {

    // 1) Generating GPX XML elements
    const Gpx = (props) => React.createElement("gpx", props);
    const Wpt = (props) => React.createElement("wpt", props);
    const Ele = (props) => React.createElement("ele", props);

    const elementXML = ReactDomServer.renderToStaticMarkup(
      <Gpx version="1.1" creator="route-builder">
        {
          this.props.markers && this.props.markers.map((marker, index) =>
            <Wpt lat={marker[0]} lon={marker[1]} key={index}>
              <Ele>{index + 1}</Ele>
            </Wpt>)

        }
      </Gpx>);

    // 2) Downloading file from browser by setting up virtual <a> element
    const element = document.createElement('a');
    element.setAttribute('href', 'data:text/xml;charset=utf-8,<?xml version="1.0"?>' + encodeURIComponent(elementXML));
    element.setAttribute('download', "output.gpx");
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }


}

const mapStateToProps = (state) => {
  return {
    markers: state.markers
  }
};

const actions = {reorderMarkers, removeMarker};


Sidebar.propTypes = {
  markers: PropTypes.array
};

export default connect(mapStateToProps, actions)(Sidebar);