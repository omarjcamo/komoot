import {REMOVE_MARKER, REORDER_MARKERS} from "../../app/reducers/constants";

export function reorderMarkers(from, to) {
  return {
    type: REORDER_MARKERS,
    from,
    to
  }
}

export function removeMarker(index) {
  return {
    type: REMOVE_MARKER,
    index
  }
}