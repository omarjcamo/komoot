import {ADD_MARKER, SET_MARKERS, SET_REFRESH_MAP} from "../../app/reducers/constants";

export function addMarker(latLng) {
  return {
    type: ADD_MARKER,
    latLng
  }
}

export function setRefreshMap(refresh) {
  return {
    type: SET_REFRESH_MAP,
    refresh
  }
}

export function setMarkers(markers) {
  return {
    type: SET_MARKERS,
    markers
  }
}