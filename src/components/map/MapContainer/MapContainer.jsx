import React, {Component} from 'react';
// import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import L from 'leaflet';
import {addMarker, setRefreshMap, setMarkers} from '../mapActions';
import 'leaflet/dist/leaflet.css';
import './MapContainer.css';

let config = {};
config.params = {
  center: [37.96713889, 22.21222222],
  zoom: 15,
  legends: true,
  infoControl: false,
  dragging: true,
  attributionControl: true
};

config.tileLayer = {
  uri: 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
  params: {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
  }
};

class MapContainer extends Component {

  constructor() {
    super();
    this.state = {
      lat: 51.505,
      lng: -0.09,
      zoom: 13,
      map: null,
      tileLayer: null,
      markers: []
    };
    this.initMap = this.initMap.bind(this);
    this.handleMapClick = this.handleMapClick.bind(this);
    this.refreshMap = this.refreshMap.bind(this);
  }

  componentDidMount() {
    this.initMap();
  }

  componentDidUpdate(prevProps) {

    if(this.props.refreshMap){
      this.setState({markers: this.props.markers}, () => this.refreshMap());
    }
  }

  refreshMap() {

    // 1) DELETING Existing markers and lines
    this.state.map.eachLayer((layer) => {
      // Selecting out the main tile layer
      if(typeof layer.options.attribution !== 'string') {
        this.state.map.removeLayer(layer);
      }
    });

    // 2) Creating Markers
    this.state.markers.forEach((marker,index) => {
      console.log('for each', marker, index);
      const icon = L.divIcon({className: 'marker-icon', html: index + 1});
      L.marker(marker, {icon: icon}).addTo(this.state.map);
    });

    // 3) Updating polyline
    if (this.state.markers.length > 1) {
      L.polyline(this.state.markers, {color: 'red'}).addTo(this.state.map);
    }

    this.props.setRefreshMap(false);
  };

  handleMapClick(e) {
    const currentMarker = [e.latlng.lat, e.latlng.lng];
    const stateMarkers = this.state.markers;

    // 1) Creating Marker Icon - calculating count number
    const icon = L.divIcon({className: 'marker-icon', html: stateMarkers.length + 1});
    L.marker(currentMarker, {icon: icon}).addTo(this.state.map);

    // 2) Storing new markers array
    stateMarkers.push(currentMarker);
    this.setState({markers: stateMarkers});

    // 3) Updating polyline adding from last marker to current
    if (stateMarkers.length > 1) {
      L.polyline(this.state.markers, {color: 'red'}).addTo(this.state.map);
    }

    // 4) Setting data in redux store
    this.props.setMarkers(stateMarkers);

  }

  initMap() {
    if (this.state.map) return;
    let map = L.map('map', config.params);
    map.on('click', this.handleMapClick);
    const tileLayer = L.tileLayer(config.tileLayer.uri, config.tileLayer.params).addTo(map);
    this.setState({map, tileLayer});
  }

  render() {
    return (
      <div className="map-container" id="mapUI">
        <div id="map"/>
      </div>
    );
  }
}


const mapStateToProps = (state, ownProps) => {
  return {markers: state.markers, refreshMap: state.refreshMap};
};

const actions = {addMarker, setRefreshMap, setMarkers};

// MapContainer.propTypes = {};

export default connect(mapStateToProps, actions)(MapContainer);