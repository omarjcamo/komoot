import React, {Component} from 'react';
import './App.css';
import Sidebar from "../../components/sidebar/Sidebar/Sidebar";
import MapContainer from "../../components/map/MapContainer/MapContainer";

class App extends Component {

  render() {
    return (
      <div className="main-app">
        <Sidebar/>
        <MapContainer/>
      </div>
    );
  }
}

export default App;