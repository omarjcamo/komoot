import {ADD_MARKER, SET_MARKERS, REORDER_MARKERS, SET_REFRESH_MAP, REMOVE_MARKER} from "./constants";

const initialState = {markers: []};

const routeBuilder = (state = initialState, action) => {
  switch (action.type) {

    case ADD_MARKER:
      return Object.assign({}, state, {
        markers: [...state.markers, action.latLng]
      });
    case SET_MARKERS:
      const tempMarkers = action.markers.slice(0);
      return Object.assign({}, state, {
        markers: tempMarkers
      });
    case REORDER_MARKERS:
      // Cloning array
      const tempReorderedMarkers = state.markers.slice(0);
      if (action.from < action.to) action.to--;
      tempReorderedMarkers.splice(action.to, 0, tempReorderedMarkers.splice(action.from, 1)[0]);
      return Object.assign({}, state, {
        markers: tempReorderedMarkers,
        refreshMap: true
      });
    case SET_REFRESH_MAP:
      return Object.assign({}, state, {
        refreshMap: action.refresh
      });
    case REMOVE_MARKER:
      return Object.assign({},state,{
        markers: state.markers.filter((value,index) => index !== action.index),
        refreshMap: true
      });
    default:
      return state
  }
};

export default routeBuilder;