import {createStore} from 'redux'

import routeBuilder from '../reducers/routeBuilder'

export const configureStore = () => {
  const store = createStore(
    routeBuilder,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

  if (process.env.NODE_ENV !== 'production') {
    if (module.hot) {
      module.hot.accept('../reducers/routeBuilder', () => {
        const newRootReducer = require('../reducers/routeBuilder').default;
        store.replaceReducer(newRootReducer);
      })
    }
  }

  return store;
};